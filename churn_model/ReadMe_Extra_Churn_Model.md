# dSights Churn Bulk API Repo #

This README would be guide for accessing  bulk APIs for Churn Model. 


Here brief explination is given as how to use Churn Bulk API using Python (Jupyter Notebook).

### How do I get set up? ###

There are 2 Main processes - 
1. Install Anaconda to access Python and Jupyter Notebook
2. Create Environment to use Bulk API


**1 Install Anaconda**

1. Install Python and Jupyter Notebook using Anaconda. 
   Download Anaconda version for your machine using following link 
   https://docs.anaconda.com/anaconda/install/

2. Install Anaconda using Downloaded version.

3. Steps to Install Anaconda are mentioned in above link.

Python and Jupyter are ready on your machine.

**2 Create environment**

1. In current directory requirements.txt is given. This file contains required python packages to use Bulkapi.

2. Open command prompt / Terminal

3. Create environment with python =3.7 as Base Module. Type below command on command prompt
   conda create --name <myenv> python==3.7

4. Type below command on Command prompt to activate environment
	conda activate <myenv>

5. Go to the path of Bulk API directory where requirements.txt is. To do this type below command on Command Prompt.
   cd <path direcory>

6. Install requirements.txt packages in environment using below command(s).
   
   conda install pip 		( It is optional command and used only if pip is not found in environment )

   pip install -r requirements.txt  

   pip install jupyter 

7. Now environment is created on your machine.

8. Access Jupyter Notebook to use Bulk API using below command.
   jupyter notebook USE_BULKAPI.ipynb

9. Workbook will open in Jupyter Notebook to use Bulk API.

10. Follow steps in Workbook to get Output.



### Who do I talk to? ###
* __info@dsighstsonline.com__



